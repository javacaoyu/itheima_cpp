// Created by 黑马程序员.
#include "iostream"
using namespace std;

/*
 * 返回值类型 函数名(参数1类型 参数1名称, 参数2类型 参数2名称, ......, 参数N类型 参数N名称)
 * {
 *      函数体;
 *
 *      return 返回值;
 * }
 *
 * // 需求：编写一个函数，接收2个int数字传入，返回两者中最大的数值
 */
int get_max(int a, int b)
{
    // 函数体（干活的代码）
    int max;        // 标记最大值
    max = a;

    if (b > max)
    {
        max = b;
    }

    // 对外提供结果（最大值，max变量的内容）
    // 语法： return 值;
    return max;
}

int main()
{
    // 函数的使用在main函数内编写
    // 调用函数：函数名称(传入数据);
    int max = get_max(3, 5);
    cout << "函数的结果是：" << max << endl;

    int max2 = get_max(11, 6);
    cout << "函数2次的结果是：" << max2 << endl;
    return 0;
}
