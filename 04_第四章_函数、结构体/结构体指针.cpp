// Created by 黑马程序员.
#include "iostream"
using namespace std;

int main()
{
    struct Student
    {
        string name;
        int age;
        string gender;
    };

    // 先创建一个标准的结构体对象（静态内存管理）
    struct Student stu = {"周杰轮", 11, "男"};
    // 创建结构体的指针，指向结构体对象的地址
    struct Student * p = &stu;
    // 通过结构体指针，访问结构体的成员，要使用的符号是：->
    cout << "结构体中成员的name：" << p->name << endl;
    cout << "结构体中成员的age：" << p->age << endl;
    cout << "结构体中成员的gender：" << p->gender << endl;


    // 通过new操作符，申请结构体的空间
    struct Student * p2 = new Student {"林军杰", 21, "男"};
    cout << "结构体2中成员的name：" << p2->name << endl;
    cout << "结构体2中成员的age：" << p2->age << endl;
    cout << "结构体2中成员的gender：" << p2->gender << endl;

    delete p2;


    return 0;
}
