// Created by 黑马程序员.
#include "iostream"
using namespace std;

// 功能：对小美说5次我喜欢你
void i_like_you()   // ()是需要写的
{
    for (int i=0; i<5; i++)
    {
        cout << "小美，我喜欢你！！！" << endl;
    }
}

int main()
{
    // 调用的时候()也是需要写的
    i_like_you();
    return 0;
}
