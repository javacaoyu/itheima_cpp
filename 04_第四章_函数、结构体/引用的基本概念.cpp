// Created by 黑马程序员.
#include "iostream"
using namespace std;

/*
 * 数据类型& 引用名 = 被引用变量;
 *
 * 对引用的操作等同于操作被引用变量。
 */
int main()
{
    int a = 10;
    int& b = a;

    cout << "a=" << a << endl;
    cout << "b=" << b << endl;
    a = 20;
    cout << "a=" << a << endl;
    cout << "b=" << b << endl;
    b = 30;
    cout << "a=" << a << endl;
    cout << "b=" << b << endl;

    int num1 = 10;
    int& num2 = num1;

    double d1 = 11.11;
    double& d2 = d1;
    cout << "d1=" << d1 << endl;
    cout << "d2=" << d2 << endl;
    d2 = 22.22;
    cout << "d1=" << d1 << endl;
    cout << "d2=" << d2 << endl;

    return 0;
}
