// Created by 黑马程序员.
#include "iostream"
using namespace std;

int main()
{
    struct Student
    {
        string name;
        int age = 11;
        string addr;
    };

    for (int i = 1; i < 6; i++)
    {
        cout << "正在录入学生" << i << "的信息：" << endl;
        Student stu;
        cout << "请输入姓名："; cin >> stu.name; cout << endl;
        cout << "请输入地址："; cin >> stu.addr; cout << endl;
        cout << "学生" << i << "信息录入完成。\n" << endl;

        cout << "学生" << i << "信息核对：";
        cout << "姓名：" << stu.name << ", ";
        cout << "年龄：" << stu.age << ", ";
        cout << "地址：" << stu.addr << "。" << endl;
    }

    return 0;
}
