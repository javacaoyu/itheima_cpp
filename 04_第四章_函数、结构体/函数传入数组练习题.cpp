// Created by 黑马程序员.
#include "iostream"

using namespace std;

/*
 * 编写一个函数，接收数组传入，对数组进行升序排序
 * - 传入参数需带有数组长度
 * - 没有返回值
 */

void sort_arr(int arr[], int length)
{
    int min, min_index;

    for (int i = 0; i < length - 1; i++)
    {
        for (int j = i; j < length; j++)
        {
            // 本次内层循环的第一个元素直接放入min和记录min index
            if (j == i)
            {
                min = arr[j];
                min_index = j;
            }

            // 非本次内层循环的第一个元素，就要比较大小
            if (arr[j] < min)
            {
                min = arr[j];
                min_index = j;
            }
        }

        int tmp;
        tmp = arr[i];
        arr[i] = arr[min_index];
        arr[min_index] = tmp;
    }
}


int main()
{
    int arr[] = {11, 33, 55, 99, 66, 77, 44, 22, 5, 3};
    sort_arr(arr, 10);

    for (int i = 0; i < 10; i++) {
        cout << arr[i] << endl;
    }
    return 0;
}
