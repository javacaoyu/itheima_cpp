// Created by 黑马程序员.
#include "iostream"
using namespace std;

void say_hello(string name)
{
    cout << name << ", 你好，我是黑马程序员！" << endl;
}

int main()
{
    say_hello("周杰轮");
    say_hello("林军杰");
    return 0;
}
