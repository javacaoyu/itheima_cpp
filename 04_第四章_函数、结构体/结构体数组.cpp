// Created by 黑马程序员.
#include "iostream"
using namespace std;

int main()
{
    struct Student
    {
        string name;
        int age;
        string gender;
    };

    struct Student arr[3];         // 结构体数组对象的声明
    arr[0] = {"周杰轮", 11, "男"};
    arr[1] = {"林军杰", 11, "男"};
    arr[2] = {"蔡依临", 11, "女"};

    for (int i=0; i<3; i++)
    {
        cout << "当前下标：" << i << "姓名是：" << arr[i].name << endl;
        cout << "当前下标：" << i << "年龄是：" << arr[i].age << endl;
        cout << "当前下标：" << i << "性别是：" << arr[i].gender << endl;
        cout << endl;
    }

    // 数组的声明和赋值同步写法
    struct Student arr2[2] = {
            {"蔡依临2", 11, "女"},
            {"周杰轮2", 11, "男"}
    };

    for (int i=0; i<2; i++)
    {
        cout << "结构体数组2的下标：" << i << "的name:" << arr2[i].name << endl;
        cout << "结构体数组2的下标：" << i << "的age:" << arr2[i].age << endl;
        cout << "结构体数组2的下标：" << i << "的gender:" << arr2[i].gender << endl;
    }

    return 0;
}
