// Created by 黑马程序员.
#include "iostream"
using namespace std;

// 功能：对小美说5次我喜欢你
void self_introduction()   // ()是需要写的
{
    cout << "大家好，我是黑马程序员！\n";
    cout << "学IT来黑马，我是曹老师，带你畅游C++。" << endl;
}

int main()
{
    // 调用的时候()也是需要写的
    self_introduction();
    return 0;
}
