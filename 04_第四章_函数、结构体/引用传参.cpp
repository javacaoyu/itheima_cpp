// Created by 黑马程序员.
#include "iostream"
using namespace std;

/*
 * 引用传参：
 * void func(int &a, int &b);
 */
void switch_num1(int a, int b)      // 值传递
{
    int tmp;
    tmp = a;
    a = b;
    b = tmp;
}

void switch_num2(int * a, int * b)  // 地址传递
{
    int tmp;
    tmp = *a;
    *a = *b;
    *b = tmp;
}

void switch_num3(int &a, int &b)
{
    int tmp;
    tmp = a;
    a = b;
    b = tmp;
}


int main()
{
    int x = 1;
    int y = 2;
    switch_num3(x, y);
    cout << "x=" << x << endl;
    cout << "y=" << y << endl;

    return 0;
}
