// Created by 黑马程序员.
#include "iostream"
using namespace std;

/*
 * 喜欢小美，正在追求中，每天的追求方案有3种：
 * 1. 送早餐、送花、说喜欢
 * 2. 送花、说喜欢、邀请一起看电影
 * 3. 邀请一起看电影、送花、说喜欢
 *
 * 用函数的思想，模拟这些动作。
 */

void send_food()
{
    cout << "小美，我给你买了早餐！" << endl;
}

void send_floor()
{
    cout << "小美，我给你买了玫瑰花，你真好看。" << endl;
}

void say_love()
{
    cout << "小美，我很喜欢你！" << endl;
}

void watch_movie()
{
    cout << "小美，我们一起看电影去吧。" << endl;
}

void i_like_you(int num)
{
    switch (num)
    {
        case 1:
            send_food();
            send_floor();
            say_love();
            break;
        case 2:
            send_floor();
            say_love();
            watch_movie();
            break;
        case 3:
            watch_movie();
            send_floor();
            say_love();
            break;
        default:
            cout << "今天不追求小美了，去打球去。！" << endl;
    }
}

int main()
{
    cout << "今天天气不错，执行方案3追求小美" << endl;
    i_like_you(3);

    cout << "第二天，天气也不错，执行方案2" << endl;
    i_like_you(2);

    return 0;
}
