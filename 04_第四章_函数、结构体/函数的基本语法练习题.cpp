// Created by 黑马程序员.
#include "iostream"
using namespace std;

int find_min(int a, int b, int c)
{
    int min = a;
    if (b < min)
    {
        min = b;
    }
    if (c < min)
    {
        min = c;
    }

    return min;
}

int main()
{
    int min = find_min(1, 3, 5);
    cout << "最小数字是：" << min << endl;
    return 0;
}
