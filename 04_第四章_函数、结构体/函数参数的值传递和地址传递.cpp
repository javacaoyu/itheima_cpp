// Created by 黑马程序员.
#include "iostream"
using namespace std;

void switch_num(int a, int b)
{
    int tmp = a;
    a = b;
    b = tmp;
}

void switch_num_pointer(int *a, int *b)
{
    int tmp = *a;
    *a = *b;
    *b = tmp;
}

int main()
{
    int x = 1;
    int y = 2;
    switch_num(x, y);

    cout << "x=" << x << endl;
    cout << "y=" << y << endl;

    switch_num_pointer(&x, &y);
    cout << "x=" << x << endl;
    cout << "y=" << y << endl;
    return 0;
}
