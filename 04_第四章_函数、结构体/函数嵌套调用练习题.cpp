// Created by 黑马程序员.
#include "iostream"
using namespace std;

int get_min(int a, int b)
{
    if (a < b)
    {
        return a;
    }

    return b;
}

int get_max(int a, int b)
{
    if (a > b)
    {
        return a;
    }

    return b;
}

struct MinAndMax
{
    int min;
    int max;
};

struct MinAndMax get_min_and_max(int a, int b)
{
    int min = get_min(a, b);
    int max = get_max(a, b);
    struct MinAndMax v = {min, max};

    return v;
}

int main()
{
    struct MinAndMax v = get_min_and_max(3, 5);
    cout << "结构体中的min：" << v.min << endl;
    cout << "结构体中的max：" << v.max << endl;
    return 0;
}
