// Created by 黑马程序员.
#include "iostream"
using namespace std;

int main()
{
    struct Student
    {
        string name;
        int age = 11;
        string gender = "男";
    };
    struct Student arr1[3] = {{"周杰轮"}, {"林军杰"}, "王力鸿"};
    struct Student * p1 = arr1;     // 数组的对象本质上就是地址（指向数组的第一个元素的位置）
    cout << "数组的第一个元素中记录的name是：" << p1[0].name << endl;
    cout << "数组的第二个元素中记录的name是：" << p1[1].name << endl;
    cout << "数组的第三个元素中记录的name是：" << p1[2].name << endl;

    // 通过new操作符，自行申请结构体数组的空间（可以通过delete回收）
    struct Student * p2 =
            new Student[3] {{"周杰轮"}, {"林军杰"}, {"王力鸿"}};

    cout << "数组2的第一个元素中记录的name是：" << p2[0].name << endl;
    cout << "数组2的第二个元素中记录的name是：" << p2[1].name << endl;
    cout << "数组2的第三个元素中记录的name是：" << p2[2].name << endl;

    delete[] p2;

    return 0;
}
