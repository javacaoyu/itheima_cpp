// Created by 黑马程序员.
#include "iostream"
using namespace std;

/*
 * 声明语法：
 *      struct 结构体类型名
 *      {
 *          成员类型 成员名;
 *          ...
 *          ...
 *      }
 *
 * 特点：
 *      一个结构体类型，可以包含多个成员（类似数组元素），每个成员类型不限
 *      可以做到一批不同类型的数据，混装在一个结构体内。
 *
 */

int main()
{
    struct Student          // 一种新的数据类型（是我们自己创建的）
    {
        string name;        // 成员1 表示姓名
        int age;            // 成员2 表示年龄
        string gender;      // 成员3 表示性别
    };

    // 结构体变量的声明，可以在前面带上struct关键字（可以省略不写）
    // 建议写上，可以清晰的知道变量是自定义结构体类型的
    struct Student stu;            // 结构体变量
    stu = {"周杰轮", 11, "男"};

//    cout << stu << endl; 结构体变量是一个整体的包装，无法直接cout输出
    // 需要访问它的每一个成员进行输出，访问语法：结构体变量.成员名称
    cout << stu.name << endl;
    cout << stu.age << endl;
    cout << stu.gender << endl;

    struct Student stu2 = {"林俊介", 11, "男"};
    cout << "2号学生的姓名是：" << stu2.name << endl;
    cout << "2号学生的年龄是：" << stu2.age << endl;
    cout << "2号学生的性别是：" << stu2.gender << endl;
    return 0;
}
