// Created by 黑马程序员.
#include "iostream"
using namespace std;

int main()
{
    struct Student
    {
        string name;
        int age;
        string gender;
    };

    struct Student arr[5];
    for (int i = 0; i < 5; i++)
    {
        cout << "请输入第" << i+1 << "位学生的信息" << endl;
        cout << "姓名："; cin >> arr[i].name;
        cout << "年龄："; cin >> arr[i].age;
        cout << "性别："; cin >> arr[i].gender;
    }
    cout << "信息录入完成。\n" << endl;

    for (int i = 0; i < 5; i++)
    {
        cout << "学生：" << i+1 << "，姓名：" << arr[i].name << endl;
        cout << "学生：" << i+1 << "，年龄：" << arr[i].age << endl;
        cout << "学生：" << i+1 << "，性别：" << arr[i].gender << endl;
        cout << endl;
    }

    return 0;
}
