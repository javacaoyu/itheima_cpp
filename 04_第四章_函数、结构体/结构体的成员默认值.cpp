// Created by 黑马程序员.
#include "iostream"
using namespace std;

int main()
{
    struct Student
    {
        string name;        // 成员1，姓名
        string major_code = "003032";   // 成员2专业代码，拥有默认值003032
        int dormitory_num = 1;          // 成员3宿舍楼号，默认是1号楼
    };

    struct Student s1 = {"周杰轮"};
    struct Student s2 = {"林军杰", "003001", 3};

    cout << "学生1的姓名：" << s1.name << endl;
    cout << "学生1的专业代码：" << s1.major_code << endl;
    cout << "学生1的宿舍楼号：" << s1.dormitory_num << endl;
    cout << endl;
    cout << "学生2的姓名：" << s2.name << endl;
    cout << "学生2的专业代码：" << s2.major_code << endl;
    cout << "学生2的宿舍楼号：" << s2.dormitory_num << endl;

    return 0;
}
