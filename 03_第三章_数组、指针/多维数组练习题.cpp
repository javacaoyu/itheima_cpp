// Created by 黑马程序员.
#include "iostream"
#include "random"
using namespace std;


int get_random_num(int min, int max) {
    // 创建一个随机数生成器
    random_device rd;
    mt19937 gen(rd());


    // 定义一个均匀分布的整数范围
    uniform_int_distribution<> dis(min, max);

    // 生成一个随机数并输出
    int random_number = dis(gen);
    return random_number;
}


int main()
{
    string names[] ={
            "Landon", "Avery", "Kamden", "Bentley", "Finnegan", "Nash", "Emmett",
            "Greyson", "Noah", "Jace", "Jaxton", "Sawyer", "Zachary", "Eli",
            "Keegan", "Lincoln", "Isaac", "Asher", "Declan", "Theo", "Levi",
            "Dominic", "Austin", "Wyatt", "Carter", "Logan", "Luke", "Max",
            "Ethan", "Miles", "Oliver", "Hudson", "Owen", "William", "Joshua",
            "Benjamin", "Henry", "Lucas", "Alexander", "Jackson", "Mason",
            "Grayson", "Ryder", "Elijah", "Liam", "Caleb", "Thomas",
            "Cooper", "Hunter", "Connor"
    };
    return 0;
}
