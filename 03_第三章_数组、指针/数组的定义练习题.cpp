// Created by 黑马程序员.
#include "iostream"
#include "random"
using namespace std;


int get_random_num(int min, int max)
{
    // 创建一个随机数生成器
    random_device rd;
    mt19937 gen(rd());


    // 定义一个均匀分布的整数范围
    uniform_int_distribution<> dis(min, max);

    // 生成一个随机数并输出
    int random_number = dis(gen);
    return random_number;
}
/*
 * 获得一个随机数（1-10），创建一个10元素的数组对象。
 * 由用户进行键盘输入，提供数组10个元素的值
 * 使用for循环挨个取出数组的元素
 * 判断每个元素是否和随机数相等
 * 最终输出用户输入的10个数字中，猜对了几个
 */

int main()
{
    // 1. 获得随机数
    int num = get_random_num(1, 10);
    cout << num << endl;

    // 2. 需要用户输入10次数字，将内容存入数组中
    int arr[10];    // 10个元素的int数组
    for (int i = 0; i < 10; i++)
    {
        cout << "请第" << i + 1 << "次输入数字：";
        cin >> arr[i];
    }

    int result = 0;
    // 3. 去判断数组每个元素是否和随机数相等
    for (int i = 0; i < 10; i++)
    {
        if (arr[i] == num)
        {
            result++;
        }
    }

    cout << "用户最终猜对了：" << result << "次" << endl;
    return 0;
}
