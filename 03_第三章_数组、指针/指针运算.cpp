// Created by 黑马程序员.
#include "iostream"
using namespace std;

/*
 * 指针进行加减计算是可以的
 *
 * 每次加减n， 就是对内存地址进行 n * 类型大小 的加减操作
 * 如int 类型指针，  +1 是内存地址+4（字节），因为int类型占用4字节大小
 */
int main()
{
    int num = 10;
    int *p = &num;

    cout << "指针变量p中记录的地址是：" << p << endl;
    p++;
    cout << "指针变量p进行了+1操作，记录的地址是：" << p << endl;

    double num2 = 10;       // double类型占用8个字节
    double * p2 = &num2;
    cout << p2 << endl;
    p2 += 4;
    cout << p2 << endl;

    int v[] = {1, 2, 3, 4, 5};  // 内存连续排列的，每个元素的地址差值就是4字节（int数组）
    int *vp = v;    // 指针中记录了数组0下标元素的地址

    cout << "数组的第一个元素是：" << *vp << endl;
    cout << "数组的第一个元素是：" << v[0] << endl;

    cout << "数组的第二个元素是：" << *(vp+1) << endl;    // vp+1正好内存地址+4字节，正好就是下一个元素的地方
    cout << "数组的第二个元素是：" << v[1] << endl;

//    v[2] = 33;
    *(vp+2) = 33;
    cout << "数组的第三个元素是：" << v[2] << endl;
    cout << "数组的第三个元素是：" << *(vp+2) << endl;

    return 0;
}
