// Created by 黑马程序员.
#include "iostream"
using namespace std;

/**
 * 指针变量语法：
 *     数据类型 * 变量名 = 内存地址;
 * `*` 在`声明`时，表示指针变量
 * `*` 在`使用`时，取指针变量指向内存区域内的`数值`
 * `&` 取变量的内存地址，如&num，得到num变量的内存地址
 */

int main()
{
    int num = 10;   // 标准的int类型变量，存放了一个int值 10

    int * p;        // 声明了一个p指针变量，这个指针变量中记录的地址（内存区域）存的是int
    // 指针变量（如上的p变量），只用来记录内存地址
    p = &num;       // 将num变量的地址，赋予了p指针变量

    cout << "&num取地址的结果是：" << &num << endl;
    cout << "指针变量p中记录的地址是：" << p << endl;
    cout << "取指针变量记录地址中存放的数值： " << *p << endl;

    // 直接展示对内存的操纵
    *p = 20;        // 等同于 num = 20;
    cout << "*p赋值后，结果：" << *p << endl;
    cout << "num的结果：" << num << endl;
    return 0;
}
