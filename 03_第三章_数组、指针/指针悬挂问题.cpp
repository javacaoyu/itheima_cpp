// Created by 黑马程序员.
#include "iostream"
using namespace std;

/*
 * 1. 不要轻易进行指针之间的赋值。
 * 2. 一旦用delete，要确保真的这个内存区域没人用了。
 */

int main()
{
    int * p1 = new int;
    *p1 = 10;

    int * p2 = p1;      // 指针之间的相互赋值

    cout << "p1指针指向的内存区域的值是：" << *p1 << endl;
    delete p1;
    cout << "p2指针指向的内存区域的值是：" << *p2 << endl;
    return 0;
}
