// Created by 黑马程序员.
#include "iostream"
using namespace std;

/*
 * 获取数组长度： sizeof(数组对象) / sizeof(数组某个元素)
 *
 * 高级for循环获取数组每个元素：
 * for (元素类型 临时变量: 数组变量)
 * {
 *      临时变量即数组元素;
 * }
 *
 */
int main()
{
    int arr[] = {1, 2, 3, 4, 5};

    // while
    int i = 0;  // 控制因子（循环中作为下标）从0开始
    while (i < sizeof(arr) / sizeof(arr[0]))
    {
        cout << "while循环取出内容：" << arr[i] << endl;
        i++;
    }

    // for
    for (int i = 0; i < sizeof(arr) / sizeof(arr[0]); i++)
    {
        cout << "for循环取出内容：" << arr[i] << endl;
    }

    // for 的高级写法
    for (int element: arr)
    {
        cout << "高级for循环取出内容：" << element << endl;
    }

    return 0;
}
