// Created by 黑马程序员.
#include "iostream"

using namespace std;

/*数组降序排序（从大到小）*/

int main()
{
    int * pArr = new int[10]{3, 5, 1, 11, 99, 66, 22, 2, 8, 6};

    int max;            // 变量用于记录最大值，也用于进行最大值的比较
    int max_index;      // 用于记录最大值所在的下标
    // 外层for循环
    for (int i = 0; i < 9; i++)
    {   // 循环执行9次，每一次都在当前选定的范围内，找出本次的最小值并放入开头位置
        for (int j = i; j < 10; j++)
        {
            if (j == i)
            {// 在第一个元素的时候
                max = pArr[j];      // 将第一个元素记录到max中
                max_index = j;      // 将对应的下标记录到max_index中
            }
            // 开始比较
            if (pArr[j] > max)
            {
                max = pArr[j];
                max_index = j;
            }
        }

        // 进行交换，将当前最大值的值和第一个元素（相对，第一次是0下标，第二次是1下标，第三次是2下标。。。）进行位置交换
        // 第一个元素的下标，用i即可
        int tmp = pArr[i];          // 将i下标元素的值，通过tmp临时变量进行备份
        pArr[i] = pArr[max_index];
        pArr[max_index] = tmp;
    }

    for (int i = 0; i < 10; i++)
    {
        cout << pArr[i] << ",";
    }
    return 0;
}
