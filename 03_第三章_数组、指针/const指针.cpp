// Created by 黑马程序员.
#include "iostream"
using namespace std;

/*
 * const表示常量（不可变）
 *
 * 1. 指向const（常量）的指针
 * const int * p;
 *
 * 2. const（常量）指针
 * int * const p = 地址;            指针指向不可改，必须初始化
 *
 * 3. 指向const的const指针
 * const int * const p = 地址;      指针指向不可改，必须初始化
 */
int main()
{
    int num1 = 10, num2 = 100;
    // 1. 指向const的指针， 指向可变，数据不可变
//    const int * p = &num1;
//    cout << "指针p当前指向的数据是：" << *p << endl;
////    *p = 20;
//    p = &num2;
//    cout << "指针p当前指向的数据是：" << *p << endl;

    // 2. const指针，指向不可变，数据可变
//    int * const p = &num1;
//    cout << "指针p当前指向的数据是：" << *p << endl;
//    *p = 20;
//    cout << "指针p当前指向的数据是：" << *p << endl;
//    p = &num2;

    // 3. 指向const的const指针，指向不可变，数据不可变
    const int * const p = &num1;
    cout << "指针p当前指向的数据是：" << *p << endl;

//    p = &num2;
//    *p = 20;
    int * pArr = new int[10] {3, 5, 1, 11, 99, 66, 22, 2, 8, 6};
    {1, 2, 3, 5, 6, 8, 11, 22, 66, 99};
    return 0;
}
