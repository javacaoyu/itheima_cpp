// Created by 黑马程序员.
#include "iostream"
using namespace std;

/* 在下标1和3插入数字：11和66 */

int main()
{
    // 示例数组
    int * pArr = new int[5] {1, 3, 5, 7, 9};

    // 创建新数组
    int * pNewArr = new int[7];

    // 循环新数组，挨个进行元素填充（非插入的位置，填充老数组元素，插入位置填充新元素）
    int offset = 0;     // 偏移量用来控制新老数组元素的下标对照
    for (int i = 0; i < 7; i++)
    {
        if (i == 1)
        {
            // 走到了下标1，应当执行新元素插入
            pNewArr[i] = 11;
            offset++;       // 每插入一个新元素，offset+1
            continue;
        } else if (i == 3)
        {
            // 走到了下标3，应当执行新元素插入
            pNewArr[i] = 66;
            offset++;       // 每插入一个新元素，offset+1
            continue;
        }

        // 不是插入位置，从老数组中提取元素放入新数组中
        // 公式：老数组的元素下标 + offset = 新数组元素下标
        // 当前循环的i是新数组的元素下标
        pNewArr[i] = pArr[i-offset];
    }

    // 收尾工作
    delete[] pArr;
    pArr = pNewArr;

    // 将新数组的内容输出
    for (int i = 0; i< 7; i++)
    {
        cout << pNewArr[i] << ",";
    }

    return 0;
}
