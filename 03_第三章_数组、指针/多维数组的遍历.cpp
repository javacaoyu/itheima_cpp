// Created by 黑马程序员.
#include "iostream"

using namespace std;

/*
 * 会有多层嵌套循环
 * 每一层循环对应一个维
 * 一般for循环更为方便
 */

int main()
{
    int v1[2][3] = {
            {1, 2, 3},
            {4, 5, 6}
    };

    for (int i=0; i < sizeof(v1) / sizeof(v1[0]); i++)
    {
        // v1[0] -> {1, 2, 3};
        // v1[1] -> {4, 5, 6};
        for (int j=0; j < sizeof(v1[0]) / sizeof(v1[0][0]); j++)
        {
            cout << v1[i][j] << endl;
        }
    }

    int v2[2][2][3] = {
            {
                    {1, 2, 3},
                    {4, 5, 6}
            },
            {
                    {7, 8, 9},
                    {10, 11, 12}
            }
    };
    for (int i=0; i < sizeof(v2)/sizeof(v2[0]); i++)
    {
        // v2[0] {
        //                    {1, 2, 3},
        //                    {4, 5, 6}
        //            },
        // v2[1] {
        //                    {7, 8, 9},
        //                    {10, 11, 12}
        //            }
        for (int j=0; j < sizeof(v2[0])/sizeof(v2[0][0]); j++)
        {
            // v2[0][0] -> {1, 2, 3}
            // v2[0][1] -> {4, 5, 6}
            for (int k=0; k < sizeof(v2[0][0])/sizeof(v2[0][0][0]); k++)
            {
                cout << v2[i][j][k] << endl;
            }
        }
    }
    return 0;
}
