// Created by 黑马程序员.
#include "iostream"
using namespace std;

/* 移除下标0和5，2个元素*/

int main()
{
    int * pArr = new int[10] {3, 5, 1, 11, 99,
                              66, 22, 2, 8, 6};

    // 创建一个新数组
    int * pNewArr = new int[8];

    // for循环遍历老的数组，将需要的元素一个个的放入新数组中
    int offset = 0;         // 偏移量控制变量
    for (int i = 0; i < 10; i++)
    {
        if (i == 0 || i == 5)
        {
            // 这两个元素是不要的
            offset++;       // 每跳过一个不要的元素，offset+1，可以让后续的元素减去offset得到准确的新数组内的下标
            continue;
        }

        pNewArr[i-offset] = pArr[i];
    }

    // 将老数组的空间回收
    delete[] pArr;

    // 可选，将老数组的指针对象，指向新数组的空间
    pArr = pNewArr;

    // 将新数组的内容输出看一下
    for (int i = 0; i<8; i++)
    {
        cout << pNewArr[i] << endl;
    }

    return 0;
}
