// Created by 黑马程序员.
#include "iostream"
using namespace std;

int main()
{
    // 示例数组
    int * pArr = new int[5] {1, 3, 5, 7, 9};

    // 创建一个新的数组，将需要保留的复制到新数组中
    int * pNewArr = new int[4];

    // 循环去遍历老的数组，将需要的元素放入新数组中（不要的要跳过）
    for (int i = 0; i < 5; i++)
    {
        if (i == 2)
        {
            continue;
        }

        if (i > 2)
        {
            pNewArr[i-1] = pArr[i];
        }else
        {
            pNewArr[i] = pArr[i];
        }
    }

    // 可选
    delete[] pArr;      // 回收老数组的空间（可选，根据需要来说）

    // 可选
    pArr = pNewArr;     // 将老数组的指针指向新数组的内存空间（可选，根据需要）

    for (int i = 0; i < 4; i++)
    {
        cout << "新数组的元素是：" << pNewArr[i] << endl;
        cout << "新数组的元素是：" << pArr[i] << endl;
    }

    return 0;
}
