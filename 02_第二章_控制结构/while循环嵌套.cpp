// Created by 黑马程序员.
#include "iostream"
#include "random"
using namespace std;


int get_random_num(int min, int max)
{
    // 创建一个随机数生成器
    random_device rd;
    mt19937 gen(rd());


    // 定义一个均匀分布的整数范围
    uniform_int_distribution<> dis(min, max);

    // 生成一个随机数并输出
    int random_number = dis(gen);
    return random_number;
}

int main()
{
    /**
     * while (条件)
     * {
     *      // 循环体
     *      ...
     *      ...
     *
     *      while (条件)
     *      {
     *
     *      }
     * }
     */
    bool is_continue = true;
    while (is_continue) {
        cout << "今天又是新的一天，开始向小美表白" << endl;

        // 每一天表白的流程 每一次都送三朵玫瑰花
        int i = 0;  // 内层循环控制因子
        while (i < 3) {
            cout << "送给小美一朵玫瑰花！！！" << endl;
            i++;    // 内层循环控制因子的更新
        }

        cout << "小美，我喜欢你！" << endl;

        int num = get_random_num(1, 20);
        if (num == 1) { // 模拟5%的几率
            // 成功
            is_continue = false;
        }

        cout << endl;
    }
    cout << "总算表白成功了！" << endl;

    return 0;
}
