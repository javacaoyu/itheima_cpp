// Created by 黑马程序员.
#include "iostream"
#include "random"
using namespace std;

int get_random_num(int min, int max)
{
    // 创建一个随机数生成器
    random_device rd;
    mt19937 gen(rd());


    // 定义一个均匀分布的整数范围
    uniform_int_distribution<> dis(min, max);

    // 生成一个随机数并输出
    int random_number = dis(gen);
    return random_number;
}

int main()
{
    /**
     * 1. 无限次机会
     * 2. 提示大了或者小了
     * 3. 提示总共猜了多少次
     */
    // 1. 得到一个随机数1-100范围
    int num = get_random_num(1, 100);
//    cout << num << endl;

    // 2. 提示用户输入数字进行猜测
    int guess_num;
    cout << "请第一次猜测一下数字：" << endl;
    cin >> guess_num;

    int guess_count = 1;
    // 3. 判断，如果猜测继续猜测
    while (guess_num != num) {  // 为true 表示猜错
        guess_count++;            // 一旦猜测，猜测的次数就+1
        // 1. 判断条件上面就是
        // 2. 循环控制因子就是guess_num变量
        // 3. 控制因子的更新，是来自于如果猜错，继续要求用户输入猜测的数字
        cout << "不好意思你猜错了" << endl;

        // 提示用户大了或小了
        if (guess_num > num) {
            cout << "你猜的有点【大】，请重新猜测：" << endl;
            cin >> guess_num;       // 控制因子的更新
        }else {
            cout << "你猜的有点【小】，请重新猜测：" << endl;
            cin >> guess_num;       // 控制因子的更新
        }
    }

    // 能够执行到这里，一定是猜对了
    cout << "恭喜你猜对了，一共猜测了：" << guess_count << "次！" << endl;

    return 0;

}
