// Created by 黑马程序员.
#include "iostream"
#include "random"
using namespace std;

/**
 * for循环猜数字，提供无限次机会，猜错了提示大了或小了
 * 使用bool型作为条件判断依据，猜对了置为false，猜错了依旧是true
 * 不需要循环因子更新语句（在循环体内，通过if判断来修改）
 */
int get_random_num(int min, int max)
{
    // 创建一个随机数生成器
    random_device rd;
    mt19937 gen(rd());


    // 定义一个均匀分布的整数范围
    uniform_int_distribution<> dis(min, max);

    // 生成一个随机数并输出
    int random_number = dis(gen);
    return random_number;
}


int main()
{
    // 获取一个随机数字
    int num = get_random_num(1, 10);

    // 要求用户猜测（第一次）
    int guess_num;
    cout << "请猜测数字：" << endl;
    cin >> guess_num;

    // for循环去做判断并继续执行猜测流程了
    for (bool is_continue = true; is_continue; )
    {
        // 对猜测的内容做判断
        if (guess_num == num)
        {
            // 猜对了
            cout << "恭喜你猜对了！" << endl;
            is_continue = false;        // 手动更改循环因子的值。
        }
        else if (guess_num > num)
        {
            cout << "你猜的大了！" << endl;
            cout << "请重新猜测：" << endl;
            cin >> guess_num;           // 要求用户再次猜测
        }
        else
        {
            cout << "你猜的小了！" << endl;
            cout << "请重新猜测：" << endl;
            cin >> guess_num;           // 要求用户再次猜测
        }
    }
    return 0;
}
