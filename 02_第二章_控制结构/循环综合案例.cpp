// Created by 黑马程序员.
#include "iostream"
#include "random"
using namespace std;

/**
 * 某公司，账户余额有1W元，给20名员工发工资。
 * 员工编号从1到20，从编号1开始，依次领取工资，每人可领取1000元
 * 领工资时，财务判断员工的绩效分（1-10）（随机生成），如果低于5，不发工资，换下一位
 * 如果工资发完了，结束发工资。
 *
 * 提示：
 * 使用循环对员工依次发放工资
 * continue用于跳过员工，break直接结束发工资
 */

int get_random_num(int min, int max)
{
    // 创建一个随机数生成器
    random_device rd;
    mt19937 gen(rd());


    // 定义一个均匀分布的整数范围
    uniform_int_distribution<> dis(min, max);

    // 生成一个随机数并输出
    int random_number = dis(gen);
    return random_number;
}


int main()
{
    // 1. 定义一个变量，记录余额
    int money = 10000;

    // 2. for循环进入发工资流程
    for (int id = 1; id <= 20; id++)
    {
        // 如果工资发完了，结束发工资
        if (money <= 0)
        {
            cout << "工资发放完成，下一个月在来。" << endl;
            break;
        }

        // 随机得到绩效分
        int score = get_random_num(1, 10);
        if (score < 5)
        {
            cout << "不好意思，员工：" << id << "，绩效分：" << score << "，小于5分，不发工资，下一位" << endl;
            continue;
        }

        // 开始发工资了
        money -= 1000;
        cout << "员工：" << id << "领取工资1000，当前余额剩余：" << money << "元" << endl;

    }

    return 0;
}
