// Created by 黑马程序员.
#include "iostream"
using namespace std;

/**
 * for (循环因子初始化语句①； ②条件判断； 循环因子变化语句④)
 * {
 *      // ③循环体，当条件表达式为真时执行
 *      code;
 *      …
 *      …
 * }
 */

int main()
{
    // 基础for循环 打印1-10的数字
//    for (int num = 1; num <= 10; num++)
//    {
//        cout << num << endl;
//    }

    // 打印1-20之间的奇数
//    for (int i = 1; i <= 20; i+=2)
//    {
//        cout << i << endl;
//    }

    // for循环()内的是三个部分，可以按照需求省略
    // 条件判断是可以省略的（不建议这么做）
//    for (int i = 0; ; ++i)
//    {
//        cout << i << endl;
//    }

    // 循环控制因子的创建可以省略
    // 尽管可以省略，但是一般你离不开循环控制因子，所以还是不要省略了
//    int num = 0;
//    for (; num < 100; num++)
//    {
//        cout << num << endl;
//    }

    // 循环控制因子的更新语句 也可以省略
//    // 不建议省略
//    for (int i = 0; i < 10;)
//    {
//        cout << i << endl;
//    }

    return 0;
}
