#include "iostream"
using namespace std;

// 通过cin输入数据，并记录到变量中。并对这个变量进行判断，如果大于120则输出信息，否则也输出信息
int main() {
    int num;
    cout << "请输入身高（cm）：";
    cin >> num;
    if (num > 120) {
        cout << "身高大于120cm，需要买票10元" << endl;
    } else {
        cout << "身高满足条件，免费游玩" << endl;
    }

    int arr[3][3][2];
    // 遍历arr这个三维数组
    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            for (int k = 0; k < 2; k++) {
                cout << arr[i][j][k] << " ";
            }
            cout << endl;
        }
        cout << endl;
    }
    return 0;
}
