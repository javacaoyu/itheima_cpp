// Created by 黑马程序员.
#include "iostream"
using namespace std;


int main()
{
    // 循环的控制因子
//    bool is_run = true;
//    int num = 0;        // 控制变量
//    while (is_run) {    // false就结束while循环了，true就继续执行循环体
//        cout << "hello" << endl;
//        num++;
//
//        if (num > 5) {
//            is_run = false; // 控制因子的更新
//        }
//    }

    // 我要向小美表白，每一天都表白，连续表白10天
    int day = 1;        // 循环的控制因子
    while (day <= 10)
    {
        cout << "今天是第" << day << "天，小美我喜欢你！" << endl;

        day++;          // 循环控制因子的更新
    }

    return 0;
}
