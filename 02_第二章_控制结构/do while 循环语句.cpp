// Created by 黑马程序员.
#include "iostream"
#include "random"
using namespace std;


int get_random_num(int min, int max)
{
    // 创建一个随机数生成器
    random_device rd;
    mt19937 gen(rd());


    // 定义一个均匀分布的整数范围
    uniform_int_distribution<> dis(min, max);

    // 生成一个随机数并输出
    int random_number = dis(gen);
    return random_number;
}

int main()
{
    /**
     * 需求：提供一个1-100的随机数字，让用户猜测。
     * 1. 提供无限次猜测机会
     * 2. 猜错了提示大了或者小了，猜对了程序结束
     */
    // 1. 先得到一个1-100的随机数字
    int num = get_random_num(1, 100);
    cout << num << endl;
    int guess_num;
    // 2. 让用户猜测
    do {
        cout << "请输入一个猜测的数字：" << endl;
        cin >> guess_num;

        if (guess_num > num) {
            cout << "你猜的大了" << endl;
        }else if (guess_num < num) {
            cout << "你猜的小了" << endl;
        } else {
            // 猜对了
            cout << "恭喜你，猜对了！";
        }
    } while (guess_num != num);

    return 0;
}
