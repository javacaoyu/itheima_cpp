// Created by 黑马程序员.
#include "iostream"
using namespace std;


int main()
{
    cout << "请选择直播的场景，输入对应的场景代码数字。\n1仅输出屏幕， 2输出屏幕+摄像头画面， 3仅输出摄像头：";
    int code_num;
    cin >> code_num;

    switch (code_num) {
        case 1:
            cout << "已经切换为场景1，用户只能看到屏幕画面。" << endl;
            break;
        case 2:
            cout << "已经切换为场景2，用户可看到屏幕和摄像头画面。" << endl;
            break;
        case 3:
            cout << "已经切换为场景3，用户仅可看到摄像头画面。" << endl;
            break;
        default:
            cout << "您输入的代码有误，无法识别具体场景。" << endl;
    }

    return 0;
}
