// Created by 黑马程序员.
#include "iostream"
using namespace std;

/**
 * for|while (; ;)
 * {
 *      ...
 *      for|while ()
 *      {
 *          ...
 *      }
 * }
 */

int main()
{
    // 1. 对行的控制9行
    for (int line = 1; line <= 9; line++)
    {
        // 2. 对列进行控制
        // 1x3  2x3  3x3
        // 左侧操作数从1开始，到行号结束。 右侧操作数是固定为行号
        for (int col = 1; col <= line; col++)
        {
            cout << col << "x" << line << "=" << col * line << "\t";
        }
        // 每一行之间留下回车分隔
        cout << endl;
    }
    return 0;
}
