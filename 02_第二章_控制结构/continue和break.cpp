// Created by 黑马程序员.
#include "iostream"
using namespace std;

// continue 跳过本次循环，进入循环下一次的流程
// break 直接停止所在循环的执行

int main()
{
    // 通过for循环输出1到20之间的奇数
//    for (int i = 1; i <= 20; i++)
//    {
//        if (i % 2 == 0)
//        {
//            continue;   // 跳过本次循环，进入下一次
//        }
//        cout << i << endl;
//    }

    for (int num = 1; num <= 5; num++)
    {
        int i = 1;
        while (i <= 20)
        {
            if ( i % 2 == 0)
            {
                i++;
                continue;
            }
            cout << i << endl;
            i++;
        }
    }


    // 通过for循环输出1到20的数字
//    for (int i = 1; true; i++)
//    {
//        cout << i << endl;
//        if (i == 20)
//        {
//            break;
//        }
//    }
    return 0;
}
