// Created by 黑马程序员.
#include "iostream"
using namespace std;


int main()
{
    /*
     * enum 枚举类型名称{
     * 枚举元素,
     * 枚举元素,
     * ...,
     * ...,
     * }
     */
    // 询问小朋友喜欢的颜色：红黄蓝三种选1个
//    enum Color{
//        RED,        // 默认是0
//        YELLOW,     // 默认是1
//        BLUE        // 默认是2
//    };
    enum Color{
        RED = 1,
        YELLOW = 3,
        BLUE = 5
    };

    int num;
    cout << "小朋友们你们喜欢什么颜色？1红色，3黄色，5蓝色" << endl;
    cin >> num;

    switch (num) {
        case RED:
            cout << "小朋友喜欢红色！" << endl;
            break;
        case YELLOW:
            cout << "小朋友喜欢黄色！" << endl;
            break;
        case BLUE:
            cout << "小朋友喜欢蓝色！" << endl;
            break;
        default:
            cout << "你输入的数字有误，不知道你喜欢什么颜色！" << endl;
    }

    return 0;
}
