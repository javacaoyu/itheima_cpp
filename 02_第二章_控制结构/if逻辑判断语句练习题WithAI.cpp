#include "iostream"
using namespace std;

int main()
{
    // 先定义一个变量记录年龄
    int age;

    // 输出欢迎语
    cout << "欢迎来到黑马儿童游乐场，儿童免费。" << endl;
    cout << "请输入你的年龄：" << endl;

    // 通过cin 要求输入年龄 为age变量赋值
    cin >> age;

    // 通过if判断，如果age小于18岁，则输出提示信息
    if (age < 18)
    {
        cout << "您未成年，可以免费游玩，欢迎。" << endl;
    }

    cout << "祝您游玩愉快。" << endl;

    return 0;
}
