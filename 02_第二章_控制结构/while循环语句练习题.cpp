// Created by 黑马程序员.
#include "iostream"
using namespace std;
/* 求1 - 100 的和 */

int main()
{
    // while循环，100次，每一次有一个变量在，变量值在第一次是1 第二次是2 第100次是100
    int num = 1;        // 循环控制因子
    int sum = 0;        // 累加变量
    while (num < 101)   // 循环判断条件
    {
        sum += num;     // 相当于 sum = sum + num;
        cout << "过程中num：" << num << endl;
        cout << "过程中sum：" << sum << endl;

        num++;          // 控制因子的更新
    }
    cout << "1到100的和是：" << sum << endl;
    return 0;
}
